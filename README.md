Reduced Mechanism Using Steady-State Approximation
Authors- Rohit Khare, P. Senthil Kumar, Krithika Narayanaswamy

The reduction procedure is taken from : "A General Procedure for Constructing Reduced Reaction Mechanisms with Given Independent Relations", J.Y. Chen (1987)
Short Communication, Combustion Science and Technology, 1988, Volume 57, pp.89-94

Requirements:

1) You need to install basic perl on your linux/unix system.

2) Any recent version of Matlab or Octave.

3) The mechanism file in the format specified below.

Mechanism File Format:

1) The mechanism file contains list of elementary reactions written one after the other in separate lines.

2) No additional space at the end of each reaction.

3) Every reaction should start with a reaction number followed by a colon (:) and then the reaction. For example "1f: H2 + O -> OH + H" 

4) For a reversible reaction, the reaction must be specified in the mechanism file only in one direction. 

5) The reactants and the products must be separated by a "->" sign. 

6) If two or more reactants/products are present, then they must be separated by a "+" sign.

7) The stoichiometric coefficient for any species in a reaction must be written before the species name and separated by a space. For example "5f: 2 HO2 -> H2O2 + O2"

8) If only species name is present in the reaction then it indicates that the stoichiometric coefficient for that species in that reaction is 1.

9) General format is specified below (for two reactant and two product species):

	ReactionNumber:_StoichiometricCoefficient_SpeciesName_+_StoichiometricCoefficient_SpeciesName_->_StoichiometricCoefficient_SpeciesName_+_StoichiometricCoefficient_SpeciesName

Note: Underscore sign (_) indicates a blank space. No space after the last product species. 

10) Sample mechanism file is provide with the code.

Usage:

1) Open the folder with the three perl files Print_Species.pl, Print_Matrix.pl, and Generate_Matlab.pl in terminal. 

2) Apart from the three perl files, the folder should also contain a bash file (PrepReduction) and the mechanism file. 

3) Make all the perl files executable use the command: "chmod 755 *.pl" in terminal.

4) Make the bash file "PrepReduction" executable.

5) In the terminal, execute the bash file as ./PrepReduction space mechanismFile. For example: ./PrepReduction Peters_Mech.txt

6) Three output files are generated once you execute the bash file: Species_List.txt, Stoichiometry_Matrix.txt, and Reduction.m

7) Open the Species_List.txt file and note down the line number of the species you want to set in steady state. 

8) Also note down the line numbers in which the reactions that you want to eliminate appear (i.e. one for each steady state species).

9) Edit Reduction.m file with the information in point (6) (line 20) and (7) (line 22) above.

10) Run the matlab file (compatible with octave as well).

11) The reduced mechanism along with the global reaction rates in terms of elementary reaction rates are displayed as an output.

Sample Example:

You will find a sample mechanism file (Peters_Mech.txt) along with the code. This is a short mechanism for methane which consists of 18 elementary reactions among 13 species. Based on steady state assumption, we choose to eliminate the species CH3, OH, O, CH2O, CHO, and HO2. You run the bash file by typing the following command in your terminal: "./PrepReduction Peters_Mech.txt". A file named Species_List.txt is generated from which the line numbers of the above species we choose to eliminate can be obtained i.e. 3, 5, 7, 8, 9, and 12 respectively. These line numbers must be given as an input in the Reduction.m file (line number 22). For each of the steady state species we choose to eliminate reaction numbers 3, 4, 7, 11, 12, and 18 respectively. This information must be given as an input in the Reduction.m file (line number 20). Once you feed in this information and run the matlab file, the following output will be displayed:

[R1] 2 H -> H2 

[R2] H2O + CO -> H2 + CO2 

[R3] 3 H2 + O2 -> 2 H + 2 H2O 

[R4] CH4 + 2 H + H2O -> 4 H2 + CO 


[W1] = w6 + w8 + w14 + w15 

[W2] = w9 

[W3] = w10 + w16 

[W4] = w1 + w2 

For any further querries please feel free to contact:
Rohit Khare (kharerohit999@gmail.com)
Krithika Narayanaswamy (krithika@iitm.ac.in)
