clc;
clear all;

MatrixFile = fopen('Stoichiometry_Matrix.txt','r'); %Reads the Stoichiometry_Matrix.txt file and stores it in matrix Q
formatSpec = '%d';
sizeQ = [17+1 Inf];
Q = fscanf(MatrixFile,formatSpec,sizeQ);
fclose(MatrixFile);

SpeciesFile = fopen('Species_List.txt','r'); %Reads the Species_List.txt file and stores it in an array C
formatSpec = '%s';
C = textscan(SpeciesFile,formatSpec);
fclose(SpeciesFile);

for i=1:size(C{1,1})
    Species{i,1} = C{1,1}{i,1};
end

[R,N] = size(Q);
ReactionEliminated=[]; %The user must input the reactions to eliminate within the square brackets separated by a comma (,)
R_e=length(ReactionEliminated);
SpeciesEliminated = []; %The user must input the species to eliminate within the square brackets separated by a comma (,)
S_e=length(SpeciesEliminated);
m=1;

R_ne=R-R_e;
ReactionNotEliminated = zeros(1,R_ne);
for k=1:R
    if ~ismember(k,ReactionEliminated)
        ReactionNotEliminated(1,m) = k; %Stores the reactions which are not eliminated in an array
       m = m + 1;
    end
end

n=1;
S_ne=N-S_e;
SpeciesNotEliminated = zeros(1,S_ne);
for l=1:N
    if ~ismember(l,SpeciesEliminated)
        SpeciesNotEliminated(1,n) = l; %Stores the species which are not to be set in steady state in an array
       n = n + 1;
    end
end

A = zeros(S_e,S_e);
for j=1:S_e
    for i=1:S_e
        A(i,j)=Q(ReactionEliminated(i),SpeciesEliminated(j)); %The matrix A should not be a singular matrix. If so, change the reactions you choose to eliminate for a particular steady state species
    end
end

if det(A)==0
    fprintf('Please pick another set of reactions to eliminate');
end

B = zeros(S_e,S_ne);
for j=1:S_ne
    for i=1:S_e
        B(i,j)=-Q(ReactionEliminated(i),SpeciesNotEliminated(j));
    end
        
end

F = A\B; %F = Inverse of matrix A * Matrix B

D = zeros(R_ne,S_ne);
for j=1:S_ne
    for i=1:R_ne
        D(i,j)=Q(ReactionNotEliminated(i),SpeciesNotEliminated(j));
    end    
end

E = zeros(R_ne,S_e);
for j=1:S_e
    for i=1:R_ne
        E(i,j)=-Q(ReactionNotEliminated(i),SpeciesEliminated(j));
    end
end

G_d=D-E*F;
[~,idx] = sort(G_d(:,1));
G_d1 = G_d(idx,:);

%Removing dependent reactions and species using row reduced echelon form (rref)
                  
 Atranspose = G_d1.';
[Z,RB] = rref(Atranspose);
G = Atranspose(:,RB).';
G = round(G); 
[Row_G,Col_G]=size(G);

G_T=G.';
Atranspose = G_T.';
[P,PB] = rref(Atranspose);
H = Atranspose(:,PB).';

Atranspose = -G_d'.';
[U,UB] = rref(Atranspose);
T = Atranspose(:,UB).';

H_in = H\eye(size(H));

GlobalRates=H_in*T;
GlobalRates=round(GlobalRates);
[Row_GR,Col_GR]=size(GlobalRates);

%Prints the reduced mechanism
for x=1:Row_G
    fprintf('[R%d] ',x);
    Sum_Reactant=0;
    Sum_Product=0;
    for y=1:Col_G
        if G(x,y)>0
            Sum_Reactant=Sum_Reactant+1;
            Reactant_Coefficient(1,Sum_Reactant)=G(x,y);
            Reactant_Species(1,Sum_Reactant)=SpeciesNotEliminated(y);
        elseif G(x,y)<0
            Sum_Product=Sum_Product+1;
            Product_Coefficient(1,Sum_Product)=G(x,y);
            Product_Species(1,Sum_Product)=SpeciesNotEliminated(y);
        else
            continue
        end
    end
    for a=1:Sum_Reactant
        if Reactant_Coefficient(a)==1 && a==Sum_Reactant
            fprintf('%s ',Species{Reactant_Species(a)});
        elseif Reactant_Coefficient(a)==1 && a~=Sum_Reactant
            fprintf('%s + ',Species{Reactant_Species(a)});
        elseif Reactant_Coefficient(a)>1 && a==Sum_Reactant
            fprintf('%d %s ',Reactant_Coefficient(a),Species{Reactant_Species(a)});
        elseif Reactant_Coefficient(a)>1 && a~=Sum_Reactant
            fprintf('%d %s + ',Reactant_Coefficient(a),Species{Reactant_Species(a)});
        end
    end
    fprintf('-> ');
    for b=1:Sum_Product
        if abs(Product_Coefficient(b))==1 && b==Sum_Product
            fprintf('%s ',Species{Product_Species(b)});
        elseif abs(Product_Coefficient(b))==1 && b~=Sum_Product
            fprintf('%s + ',Species{Product_Species(b)});
        elseif abs(Product_Coefficient(b))>1 && b==Sum_Product
            fprintf('%d %s ',abs(Product_Coefficient(b)),Species{Product_Species(b)});
        elseif abs(Product_Coefficient(b))>1 && b~=Sum_Product
            fprintf('%d %s + ',abs(Product_Coefficient(b)),Species{Product_Species(b)});
        end
    end
	fprintf('\n');
end

fprintf('\n');

%Prints the Global Rates
for c=1:Row_GR
    flag=0;
    fprintf('[W%d] = ',c);
    Positive_Rates=0;
    Negative_Rates=0;
    for d=1:Col_GR
        if GlobalRates(c,d)>0
            Positive_Rates=Positive_Rates+1;
            Positive_Value(1,Positive_Rates)=GlobalRates(c,d);
            Positive_Reaction(1,Positive_Rates)=ReactionNotEliminated(d);
        elseif GlobalRates(c,d)<0
            Negative_Rates=Negative_Rates+1;
            Negative_Value(1,Negative_Rates)=GlobalRates(c,d);
            Negative_Reaction(1,Negative_Rates)=ReactionNotEliminated(d);
        else
            continue
        end
    end   
    for e=1:Positive_Rates
        if Positive_Value(e)==1
            fprintf('- w%d ',Positive_Reaction(e));
            flag=1;
        elseif Positive_Value(e)>1
            fprintf('- %d w%d ',Positive_Value(e),Positive_Reaction(e));
            flag=1;
        end
    end
    for f=1:Negative_Rates
        if abs(Negative_Value(f))==1 && flag==0
            fprintf('w%d ',Negative_Reaction(f));
	    flag=2;
        elseif abs(Negative_Value(f))==1 && flag==1 || 2
            fprintf('+ w%d ',Negative_Reaction(f));
	    flag=2;
        elseif abs(Negative_Value(f))>1 && flag==0
            fprintf('%d w%d ',abs(Negative_Value(f)),Negative_Reaction(f));
	    flag=2;
        elseif abs(Negative_Value(f))>1 && flag==1 || 2
            fprintf('+ %d w%d ',abs(Negative_Value(f)),Negative_Reaction(f));
	    flag=2;
        end
    end
    fprintf('\n');
end