#!/usr/bin/perl -w
use strict;
use warnings;

#### Usage ####
#Stoichiometry Matrix
###############
if( $ARGV[0] eq '-h' || $ARGV[0] eq '-help') {
	help();
	exit; 
}

my $inFile = $ARGV[0];

open(INFILE,"<$inFile") or die "!!! Cannot open \"$inFile\"\n!!!";
my @lines = <INFILE>;
close(INFILE);

my $outFile = "Stoichiometry_Matrix.txt"; #Generates an output file with the name "Stoichiometry_Matrix.txt"
open(OUT,">$outFile") or die "!!! Cannot open \"$outFile\"\n!!!";
printSpecies();
StoichiometryMatrix();
close(OUT);
my @finalSpec;

# Sub routines "uniq" and "printSpecies" from the "Print_Species.pl" file (Please check the comments in "Print_Species.pl" file for any querries)
sub uniq {
    my %seen;
    push(@finalSpec,grep !$seen{$_}++, @_);
}

sub printSpecies {
	my $count = 0;
	my @Spec;
	
	for (my $j = 0; $j <= $#lines; $j++) {
		my $species = $lines[$j];
		my @specArr = split(' ',$species);

		for (my $i = 0; $i <= $#specArr; $i++) {
			if ($specArr[$i] =~ /^M\d+$/ || $specArr[$i] =~ /^M$/) {$count++; next;}
			$count ++;
			if ($specArr[$i] =~ /^[A-Z]/) {
				push (@Spec, $specArr[$i]);
			}			
		}
	}
	uniq(@Spec);
}

sub StoichiometryMatrix { #Sub routine for identifying the stoichiometric coefficients of a species and printing it in a matrix format
	for (my $i = 0; $i <= $#finalSpec; $i++) { #for loop runs from the first to the last species in the "Species_List.txt" file
		my $currSpec = $finalSpec[$i]; #The variable stores the species corresponding to the index i
			for (my $j = 0; $j <= $#lines; $j++) { #for loop runs from the first to the last line of the mechanism file
				my $species1 = $lines[$j];
				my @specArray = split(': ',$species1); #Splits the reaction corresponding to the line number based on a colon (:) (separates the reaction number) and stores it an array
				my @reaction = split(' -> ',$specArray[1]); #Splits the reaction (excluding the reaction number) based on arrow (->) (separates reactants and products)
				my @reactants = split(' ',$reaction[0]); #Splits the reactants based on white space and stores the elements in an array
				my @products = split(' ',$reaction[1]); #Splits the products based on white space and stores it in an array
				my $flag=0;
					for (my $k=0;$k<=$#reactants;$k++) { #for loop runs through the elements of the reactants
						if ($reactants[$k] =~ m/^$currSpec$/i) { #Ckeck for species match
							if ($k == 0) {print OUT "1\t\t"; 
							}
							elsif ($reactants[$k-1] =~ /^[0-9]/) { #Prints stoichiometry coefficient other than 1
								print OUT "$reactants[$k-1]\t\t";
							}
							else {
								print OUT "1\t\t";
							}
							$flag = 1;
						}
					}

					if ($flag == 0) { #Executes the for loop only if the species is not present in the reactant side
						for (my $l=0;$l<=$#products;$l++) { #for loop runs through the elements of the products
							if ($products[$l] =~ m/^$currSpec$/i){ #Check for species match
								if ($l == 0) {print OUT "-1\t\t";
								}
								elsif ($products[$l-1] =~ /^[0-9]/) {
									print OUT "-$products[$l-1]\t\t";
																	}		
								else {
									print OUT "-1\t\t";
									
								}
								$flag = 1;
							}
						}
					}
			if ($flag == 0) {
				print OUT "0\t\t"; #Prints zero if species is not present in the reaction
			}	
			}
	print OUT "\n";
	}
}


sub help {
	print " *************\n";
	print " *** Usage ***\n";
	print " perl Print_Matrix.pl mechfile\n";
	print " *************\n";
	print " Creates stoichiometry matrix \n";		
	print " This matrix is further used in the reduction code \n\n";
}
