#!/usr/bin/perl -w
use strict;
use warnings;

#### Usage ####
#Print Species
###############
if( $ARGV[0] eq '-h' || $ARGV[0] eq '-help') {
	help();
	exit; 
}

my $inFile = $ARGV[0];
#The mechanism file is given as an input by the user which is the INFILE
open(INFILE,"<$inFile") or die "!!! Cannot open \"$inFile\"\n!!!";
my @lines = <INFILE>; #Stores the line number of the mechanism file which is given as an input
close(INFILE);

my $outFile = "Species_List.txt"; #Generates an output file with the name "Species_List.txt"
open(OUT,">$outFile") or die "!!! Cannot open \"$outFile\"\n!!!";
printSpecies();
close(OUT);
my @finalSpec;


sub uniq { #Sub routine for sorting out the unique species and eliminating species in duplicate
    my %seen;
    push(@finalSpec,grep !$seen{$_}++, @_); #Sortes out only the unique species and stores it in an array without repeating them
}

sub printSpecies {
	my $count = 0;
	my @Spec;
	
	for (my $j = 0; $j <= $#lines; $j++) { #for loop runs from the first to the last line of the mechanism file
		my $species = $lines[$j];
		my @specArr = split(' ',$species); #Splits the reaction in the corresponding line number based on a white space and stores the elements in an array

		for (my $i = 0; $i <= $#specArr; $i++) { #for loop runs from the first to the last element of the array created above
			if ($specArr[$i] =~ /^M\d+$/ || $specArr[$i] =~ /^M$/) {$count++; next;} #Skips the third body species (M,M0,M1...) and does not include them in the final species list
			$count ++;
			if ($specArr[$i] =~ /^[A-Z]/) { #Selects the elements from the array and considers it as a species if the element name starts with an alphabet (except third body species)
				push (@Spec, $specArr[$i]);
			}			
		}
	}
	uniq(@Spec); #Sub routine "uniq" is called in here to sort out the unique species and remove the duplicate species
	print OUT $_,"\n" foreach @finalSpec; #The final species list is printed in the file "Species_List.txt"
	print OUT "\n";
}

sub help {
	print " *************\n";
	print " *** Usage ***\n";
	print " perl Print_Species.pl mechfile\n";
	print " *************\n";
	print " Creates Species List \n";	
	print " All species present in the mechanism file are printed in a column\n";		
	print " The species list is further used in the reduction code \n\n";
}
